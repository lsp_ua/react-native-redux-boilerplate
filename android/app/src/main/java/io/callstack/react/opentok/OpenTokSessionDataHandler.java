package io.callstack.react.opentok;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.react.bridge.*;
import org.json.JSONException;
import org.json.JSONObject;


public class OpenTokSessionDataHandler extends ReactContextBaseJavaModule{

    private String url;
    private Response.Listener listener;

    public OpenTokSessionDataHandler(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "OpenTokSessionHandler";
    }

    @ReactMethod
    public void getSessionData(final String url, final Promise promise) {
        RequestQueue reqQueue = Volley.newRequestQueue(getReactApplicationContext());
        reqQueue.add(new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String apiKey = response.getString("apiKey");
                    String sessionId = response.getString("sessionId");
                    String token = response.getString("token");

                    WritableMap map = Arguments.createMap();
                    map.putString("apiKey", apiKey);
                    map.putString("sessionId", sessionId);
                    map.putString("token", token);
                    promise.resolve(map);

                    Log.i("apiKey", apiKey);
                    Log.i("sessionId", sessionId);
                    Log.i("token", token);
                    Toast.makeText(getReactApplicationContext(), token, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    promise.reject(e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                promise.reject(error);
            }
        }));
    }
}
