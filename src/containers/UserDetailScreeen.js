import React, {Component} from 'react';
import AppHeader from '../components/AppHeader';
import  {UserDetailScreenTitle} from '../common/AppParams';

class UserDetailScreeen extends Component {

    constructor(props) {
        super(props);
    }

    handleBack = () => {
        const {navigate} = this.props;
        navigate({type: 'pop'});
    };

    render(){
        return(<AppHeader name={UserDetailScreenTitle} leftArrow={this.handleBack}/>);
    }
}

export default UserDetailScreeen;