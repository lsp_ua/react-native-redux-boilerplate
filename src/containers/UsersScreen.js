
import {Button, Icon, Container, Content, List, ListItem, Text, Badge, Spinner, Input, InputGroup} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as userActions from '../actions/users/userActions';
import AppHeader from '../components/AppHeader';
import {UsersScreenTitle} from '../common/AppParams'
import * as React from "react";
import * as role from '../constants/userRoleConstants';

class UsersScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {login: ''};
    }

    componentWillMount(){
        this.handleFetchUsers();
    }

    handleFetchUsers = () => {
        this.props.userActions.fetchUsers();
    };

    handleFetchUsersByName = () => {
        const userName = this.state.login;
        if(userName.length > 0){
            this.props.userActions.fetchUsersByLoginMatching(userName);
        }
    };

    handleUserDetailScreen = () => {
        const {navigate} = this.props;
        navigate({
            type: 'push',
            key: 'user_detail'
        });
    };

    handleUserActionDialog = (e) => {
      console.log(e);
    };

    handleBack = () => {
        const {navigate} = this.props;
        navigate({type: 'pop'});
    };

    getUserRoles(userRoles) {
        let rolesArr = [];
        if (userRoles) {
            userRoles.map((role) => {
                rolesArr.push(role.listRole);
            });
        }
        return rolesArr.join(',');
    };

    badgeFromUserRoles(userRoles) {
        let rolesStr = this.getUserRoles(userRoles);
        switch (true){
            case rolesStr.includes(role.SUPERADMIN):
                return <Badge danger>{rolesStr}</Badge>;
            case rolesStr.includes(role.ADMIN):
                return <Badge warning>{rolesStr}</Badge>;
            case rolesStr.includes(role.SUPERUSER):
                return <Badge success>{rolesStr}</Badge>;
            case rolesStr.includes(role.USER):
                return <Badge info>{rolesStr}</Badge>;
            default:
                return <Badge warning>undefined user</Badge>;
        }
    };

    render() {
        let userData = this.props.users.payload;
        let userListItems = userData.map((dataItem, i) => {
            return <ListItem iconLeft key={i} onPress={this.handleUserDetailScreen}>
                <Icon name='ios-person'/>
                <Text>{dataItem.login}</Text>
                {this.badgeFromUserRoles(dataItem.userRoles)}
            </ListItem>;
        });
        return (
            <Container>
                <Content>
                    <AppHeader name={UsersScreenTitle} leftArrow={this.handleBack}/>
                    <InputGroup>
                        <Icon name='ios-person'/>
                        <Input placeholder='search by user login' onChangeText={login => this.setState({login})}/>
                    </InputGroup>
                    <Button style={{flex: 1}} block info onPress={this.handleFetchUsersByName}>Search</Button>
                    {this.props.ajax.loading ? <Spinner/> : <List>{userListItems}</List>}
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        users: state.users,
        ajax: state.ajax
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersScreen);