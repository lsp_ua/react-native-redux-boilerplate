import React, {Component} from 'react';
import AppHeader from '../components/AppHeader';
import {Container, Content, Grid, Row, Button} from 'native-base';
import {OpenTokRoomTitle} from '../common/AppParams';
import PublisherView from '../../opentok/PublisherView';
import SubscriberView from '../../opentok/SubscriberView';
import {OPENTOK_URL, OPENTOK_API_KEY, OPENTOK_SESSION_ID, OPENTOK_PUBLISHER_TOKEN, OPENTOK_SUBSCRIBER_TOKEN} from '../constants/opentokConstants';
import OpenTokSessionHandler from '../../opentok/OpenTokSessionHandler';

class OpenTokRoom extends React.Component {

    constructor(props) {
        super(props);
    }

    handleBack = () => {
        const {navigate} = this.props;
        navigate({type: 'pop'});
    };

    handleOpensessionData = () => {
        OpenTokSessionHandler.getSessionData(OPENTOK_URL)
            .then((response) => {
            console.log(response);
        })
            .then((error) => {
            console.log(error);
       })
    };

    render() {
        return (<Container>
            <Content>
                <AppHeader name={OpenTokRoomTitle} leftArrow={this.handleBack}/>
                <Grid>
                    <Row style={{ backgroundColor: 'aliceblue', height: 100 }}>
                        <Button block info  style={{flex:1}} onPress={this.handleOpensessionData}>CONNECT</Button>
                    </Row>
                    <Row style={{ backgroundColor: 'aliceblue', height: 200 }}>
                        <PublisherView
                            apiKey={OPENTOK_API_KEY}
                            sessionId={OPENTOK_SESSION_ID}
                            token={OPENTOK_PUBLISHER_TOKEN}
                            style={{ width: 400, height: 150 }}/>
                    </Row>
                    <Row style={{ backgroundColor: 'aliceblue', height: 30 }}>
                    </Row>
                    {/*<Row style={{ backgroundColor: 'aliceblue', height: 200 }}>
                        <SubscriberView
                            apiKey={OPENTOK_API_KEY}
                            sessionId={OPENTOK_SESSION_ID}
                            token={OPENTOK_SUBSCRIBER_TOKEN}
                            style={{ width: 400, height: 200 }}/>
                    </Row>*/}
                </Grid>
            </Content>
        </Container>);
    }
}
export default OpenTokRoom;