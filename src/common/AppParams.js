export const MainTitle = 'Simple react-native redux app';
export const AdminScreenTitle = 'Dashboard';
export const OpenTokRoomTitle = 'OpentokRoom';
export const UsersScreenTitle = "Users";
export const UserDetailScreenTitle  = "User detail";
export const MetricScreenTitle = "JVM metric";