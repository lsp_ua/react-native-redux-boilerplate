import {beginAjaxCall, ajaxCallSuccess, ajaxCallDetailError, ajaxCallError, revertAjaxStatusState} from '../ajax/ajaxDispatchActions';
import {fetchUsersRequest, fetchUsersByNameRequest, fetchUserRequest, fetchUsersCountRequest, removeUserRequest, saveUserRequest} from './usersDispatchActions';
import {getJWTTokenHeader} from '../../constants/jwtConstants';
import {USERS_URL} from '../../constants/urlConstants';
import AjaxObservable from 'rxjs/observable/dom/AjaxObservable';
import 'rxjs';
import {Observable} from 'rxjs/Observable';

import {AsyncStorage} from 'react-native';

export const fetchUsers = () => {
  return (dispatch) => {
      AsyncStorage.getItem('token').then(token => {
          dispatch(revertAjaxStatusState());
          dispatch(beginAjaxCall());
          AjaxObservable.ajaxGet(USERS_URL, getJWTTokenHeader(token))
              .map(data => data.response)
              .catch((err) => {
                  dispatch(ajaxCallError(err));
                  return Observable.empty();
              })
              .subscribe(data => {
                  if (data.length != 0) {
                      dispatch(ajaxCallSuccess());
                      dispatch(fetchUsersRequest(data));
                  }
              });
      }).done();
  };
};

export const fetchUserById = (userId) => {
  return (dispath) => {
    AsyncStorage.getItem('token').then(token => {
        dispath(revertAjaxStatusState());
        dispath(beginAjaxCall());
        AjaxObservable.ajaxGet(`${USERS_URL}/id/${userId}`,getJWTTokenHeader(token))
            .map(data => data.response)
            .catch((err) => {
                dispath(ajaxCallDetailError(err));
                return Observable.empty();
            })
            .subscribe(data => {
                dispath(ajaxCallSuccess());
                dispath(fetchUserRequest(data));
            });
    }).done();
  }
};

export const fetchUserByName = (userName) => {
  return (dispatch) => {
      AsyncStorage.getItem('token').then(token => {
          dispatch(revertAjaxStatusState());
          dispatch(beginAjaxCall());
          AjaxObservable.ajaxGet(`${USERS_URL}/name/${userName}`, getJWTTokenHeader(token))
              .map(data => data.response)
              .catch((err) => {
                  dispatch(ajaxCallDetailError(err));
                  return Observable.empty();
              })
              .subscribe(data => {
                  dispatch(ajaxCallSuccess());
                  dispatch(fetchUserRequest(data));
              });
      }).done();
  }
};

export const fetchUsersByLoginMatching = (userName) => {
    return (dispatch) => {
        AsyncStorage.getItem('token').then(token => {
            dispatch(revertAjaxStatusState());
            dispatch(beginAjaxCall());
            AjaxObservable.ajaxGet(`${USERS_URL}/login_matching/${userName}`, getJWTTokenHeader(token))
                .map(data => data.response)
                .catch((err) => {
                  dispatch(ajaxCallError(err));
                  return Observable.empty();
                })
                .subscribe(data => {
                    dispatch(ajaxCallSuccess());
                    dispatch(fetchUsersByNameRequest(data));
                });
        }).done();
    }
};

export const fetchUsersCount = () => {
    return (dispatch) => {
        dispatch(revertAjaxStatusState());
        dispatch(beginAjaxCall());
        AjaxObservable.ajaxGet(`${USERS_URL}/count`)
            .map(data => data.response)
            .catch((err) => {
                return Observable.empty();
            })
            .subscribe(data => {
                dispatch(ajaxCallSuccess());
                dispatch(fetchUsersCountRequest(data));
            });
    }
};

export const removeUser = (userId) => {
    return (dispatch) => {
        AsyncStorage.getItem('token').then(token => {
            dispatch(revertAjaxStatusState());
            dispatch(beginAjaxCall());
            AjaxObservable.ajaxDelete(`${USERS_URL}/id/${userId}`, getJWTTokenHeader(token))
                .catch((err) => {
                    dispatch(ajaxCallDetailError(err));
                    return Observable.empty();
                })
                .subscribe(data => {
                    dispatch(ajaxCallSuccess());
                    dispatch(removeUserRequest(data))
                });
        }).done();
    }
};

export const saveUser = (userName, userPassword) => {
    return (dispatch) => {
      AsyncStorage.getItem('token').then(token => {
          dispatch(revertAjaxStatusState());
          dispatch(beginAjaxCall());
          AjaxObservable.ajaxPost(`${USERS_URL}`, getJWTTokenHeader(token))
              .catch((err) => {
                  dispatch(ajaxCallDetailError(err));
                  return Observable.empty();
              })
              .subscribe(data => {
                  dispatch(ajaxCallSuccess());
                  dispatch(saveUserRequest())
              });
      }).done();
    }
};



