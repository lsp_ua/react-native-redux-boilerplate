import * as actionTypes from '../actionTypes';

export function fetchUsersRequest(data) {
  return {type: actionTypes.FETCH_USERS, payload: data};
}

export function fetchUsersByNameRequest(data) {
    return {type: actionTypes.FETCH_USERS_BY_NAME, payload: data};
}

export function fetchUserRequest(data){
  let userData = [];
  userData.push(data);
  return {type: actionTypes.FETCH_USER, payload: userData};
}


export function fetchUsersCountRequest(data){
 return {type: actionTypes.FETCH_COUNT_USERS, count: data};
}

export function removeUserRequest(data){
  return {type: actionTypes.REMOVE_USER, payload: data};
}

export function saveUserRequest() {
    return {type: actionTypes.SAVE_USER};
}
