import * as actionTypes from '../actionTypes';

export const beginAjaxCall = () => {
  return {type: actionTypes.BEGIN_AJAX_CALL, loading: true};
};

export const revertAjaxStatusState = () => {
  return {type: actionTypes.REVERT_AJAX_STATUS_STATE, loading: false, errorMessage: '', error: false};
};

export const ajaxCallError = (error) => {
  return {type: actionTypes.AJAX_CALL_ERROR, error: error};
};

export const ajaxCallDetailError = (error) => {
  const errorMessage = error.xhr.response;
  const errorsTemplate = `detail: ${errorMessage.detailMessage} status code: ${errorMessage.statusCode} status detail: ${errorMessage.statusDetail} ${errorMessage.developerMessage}`;
  return {type: actionTypes.AJAX_CALL_ERROR, error: true, errorMessage: errorsTemplate};
};

export const ajaxCallSuccess = () => {
  return {type: actionTypes.AJAX_CALL_SUCCESS, loading: false};
};

export const ajaxCallNoData = () => {
  return {type: actionTypes.AJAX_CALL_NO_DATA, error: true, noData: true, loading: false};
};
