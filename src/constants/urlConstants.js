import {IP} from '../../AppIPConfig';
const HTTP_MODE_URL = `http://${IP}:8082`;
const HTTPS_MODE_URL = `http://${IP}:8443`;

export const BASE_URL = HTTP_MODE_URL || HTTPS_MODE_URL;

export const API_URL = `${BASE_URL}/api`;

export const ADMIN_URL =  `${BASE_URL}/admin`;
export const METRIC_JVM_URL = `${ADMIN_URL}/metric`;
export const METRIC_STATUS_URL = `${ADMIN_URL}/status`;
export const METRIC_FULL_URL = `${ADMIN_URL}/full`;
export const METRIC_GRAPH_URL = `${ADMIN_URL}/graph`;

export const USERS_URL = `${API_URL}/users`;

