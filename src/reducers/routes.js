import { NavigationExperimental } from 'react-native';

const { StateUtils } = NavigationExperimental;

const initialState = {
  index: 0,
  routes: [{ key: 'login' }],
};

const actionsMap = {
  push(state, action) {
    return StateUtils.push(state, { key: action.key });
  },
  back(state) {
    return state.index > 0 ? StateUtils.pop(state) : state;
  },
  forward(state) {
    return state.index > 0 ? StateUtils.forward(state): state;
  },
  pop(state) {
    return state.index > 0 ? StateUtils.pop(state) : state;
  },
  jumpToIndex(state, index){
    return state.index > 0 ? StateUtils.jumpToIndex(state, index) : state;
  },
  jumpTo(state, key){
    return state.index > 0 ? StateUtils.jumpTo(state, key) : state;
  }
};

export default (state = initialState, action) => {
  const reduceFn = actionsMap[action.type];
  if (!reduceFn) return state;
  return reduceFn(state, action);
};
