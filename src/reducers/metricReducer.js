import * as types from '../actions/actionTypes';

const initMetricState = {
    payload: []
};

export default function users(state = initMetricState, action) {
    switch (action.type) {
        case types.FETCH_METRIC:
            return {...state, payload: action.payload};
        default:
            return state;
    }
}