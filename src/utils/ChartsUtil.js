export const prepareBarChartMetricData = (metricParam, data) => {
    let arr = [];
    for (let [k, v] of Object.entries(data)) {
        let data = {};
        data.name = k;
        data.v = v.value;
        arr.push(data);
    }
    let filteredArr = arr.filter(obj => obj.name.includes(metricParam));
    return filteredArr;
};

